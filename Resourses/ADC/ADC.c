/********************************************************************************
 * project     Charge controller for solar panel with MPPT algorithm            *
 *                                                                              *
 * file        ADC.c															*
 * author      Akbar Pulatov													*
 * date        13.02.2020                                                       *
 * copyright   Akbar Pulatov(C)													*
 * brief       Work with ADC for measuring voltage								*
 *                                                                              *
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/

#include "ADC.h"

/********************************************************************************
 * Used variable
 ********************************************************************************/
uint16_t tmp3 = 0;
uint16_t tmp4 = 0;
uint16_t tmp5 = 0;
uint16_t tmp6 = 0;

/********************************************************************************
 * ADC Initialization Function
 * Voltage input - ADC1 channel 3 - PA3
 * Voltage input - ADC1 channel 4 - PA4
 * Voltage input - ADC1 channel 5 - PA5
 * Voltage input - ADC1 channel 6 - PA6
 ********************************************************************************/

void ADC_Init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;     // ��������� ������������ ����� PORTA
	GPIOA->CRL   &= ~(GPIO_CRL_MODE3 | GPIO_CRL_CNF3);
	GPIOA->CRL   &= ~(GPIO_CRL_MODE4 | GPIO_CRL_CNF4);
	GPIOA->CRL   &= ~(GPIO_CRL_MODE5 | GPIO_CRL_CNF5);
	GPIOA->CRL   &= ~(GPIO_CRL_MODE6 | GPIO_CRL_CNF6);
	
	RCC->APB2ENR |=  RCC_APB2ENR_ADC1EN;    //������ ������������ ��� 
	RCC->CFGR    &= ~RCC_CFGR_ADCPRE;       //������� ��������
	ADC1->CR1     =  0;                     //����������� �������� 
	ADC1->CR2    |=  ADC_CR2_CAL;           //������ ���������� 
	while(!(ADC1->CR2 & ADC_CR2_CAL)) {}
	;  //���� ��������� ����������
	ADC1->CR2     =  ADC_CR2_JEXTSEL;       //������� ���������� ������� ������  JSWSTART
	ADC1->CR2    |=  ADC_CR2_JEXTTRIG;      //����. ������� ������ ��������������� ������
	ADC1->CR2    |=  ADC_CR2_CONT;          //����� ������������ �������������� 
	ADC1->CR1    |=  ADC_CR1_SCAN;          //����� ������������ (�.�. ��������� �������)
	ADC1->CR1    |=  ADC_CR1_JAUTO; 	 //�������. ������ ��������������� ������
	ADC1->JSQR    =  (uint32_t)(4 - 1) << 20;   //������ ���������� ������� � ��������������� ������
	ADC1->JSQR   |=  (uint32_t)3 << (5 * 0);    //����� ������ ��� ������� ��������������             
	ADC1->JSQR   |=  (uint32_t)4 << (5 * 1);    //����� ������ ��� ������� ��������������
	ADC1->JSQR   |=  (uint32_t)5 << (5 * 2);    //����� ������ ��� �������� ��������������
	ADC1->JSQR   |=  (uint32_t)6 << (5 * 3);    //����� ������ ��� ���������� ��������������
	ADC1->CR2    |=  ADC_CR2_ADON;          //�������� ���
	ADC1->CR2    |=  ADC_CR2_JSWSTART;      //��������� ������ ��������������
}

/********************************************************************************
 * ADC Polling
 ********************************************************************************/
void ADC_polling(void)
{
	tmp3 = ADC1->JDR1;      //��������� ��������� ������� �������������� (� ����� ������ ����� 3) 
	tmp4 = ADC1->JDR2;      //��������� ��������� ������� �������������� (� ����� ������ ����� 4)
	tmp5 = ADC1->JDR3;      //��������� ��������� �������� �������������� (� ����� ������ ����� 5)
	tmp6 = ADC1->JDR4;      //��������� ��������� ���������� �������������� (� ����� ������ ����� 6)
}


/********************************************************************************
 * ADC Interrupt Handler
 ********************************************************************************/

void ADC1_2_IRQHandler(void)
{
	asm("BKPT");
	ADC1->SR &= ~ADC_SR_JEOS;
	
	
	GPIOC->ODR ^= GPIO_ODR_ODR14;
	(void)ADC1->DR;
	
}


