#include "main.h"

void SYS_Init(void);
void GPIO_Init(void);
void delay(uint32_t time);

extern uint16_t tmp3;
extern uint16_t tmp4;
extern uint16_t tmp5;
extern uint16_t tmp6;

/**********************************************************************************************
 * Main Function
 **********************************************************************************************/

int main(void)
{
	SYS_Init();
	GPIO_Init();
	PWM_Init();
	ADC_Init();
	__NOP();
	
	TIM2->CCR2 = 300;
	
	while (1)
	{
		delay(100000);
		GPIOC->ODR ^= GPIO_ODR_ODR13;
		ADC_polling();
	}
}

/**********************************************************************************************
 * System(Flash, RCC) Initialization Function
 **********************************************************************************************/

void SYS_Init(void)
{
	//SysClolk = 72 MHz, => PLL = x9, SOURCE = PLL
	//Flash Latency corresponding to 72MHz SysClk
	FLASH->ACR |= (0b010 << FLASH_ACR_LATENCY_Pos);
	
	//Turn on HSE and wait until ready
	SET_BIT(RCC->CR, RCC_CR_HSEON); 
	while (!(RCC->CR & RCC_CR_HSERDY)) ;
	
	//Turn on PLL and wait until ready
	RCC->CFGR |= (0b0111 << RCC_CFGR_PLLMULL_Pos) | RCC_CFGR_PLLSRC;
	RCC->CR |= RCC_CR_PLLON;
	while (!(RCC->CR & RCC_CR_PLLRDY)) ;
	
	//AHB, APB2, APB1 prescalers
	RCC->CFGR |= (0 << RCC_CFGR_HPRE_Pos)
			  |  (0 << RCC_CFGR_PPRE2_Pos)
			  |  (0b100 << RCC_CFGR_PPRE1_Pos);
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	while (!(RCC->CFGR & RCC_CFGR_SWS_PLL)) ;
	
	RCC->CR |= RCC_CR_CSSON;
	
	//select PA8(should be configured seperately) as SYSclock output
	MODIFY_REG(RCC->CFGR, RCC_CFGR_MCO, RCC_CFGR_MCOSEL_SYSCLK);
	
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	//PA8 = MCO
	GPIOA->CRH &= ~(GPIO_CRH_CNF8 | GPIO_CRH_MODE8_Msk);
	GPIOA->CRH |= (0b11 << GPIO_CRH_MODE8_Pos) | (0b10 << GPIO_CRH_CNF8_Pos);
}

/**********************************************************************************************
 * GPIO Initialization Function
 **********************************************************************************************/

void GPIO_Init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
	
	//PC13
	GPIOC->CRH &= ~(GPIO_CRH_CNF13_Msk | GPIO_CRH_MODE13_Msk);
	GPIOC->CRH |= (0b11 << GPIO_CRH_MODE13_Pos) | (0b00 << GPIO_CRH_CNF13_Pos);
	
	//PC14
	GPIOC->CRH &= ~(GPIO_CRH_CNF14_Msk | GPIO_CRH_MODE14_Msk);
	GPIOC->CRH |= (0b11 << GPIO_CRH_MODE14_Pos) | (0b00 << GPIO_CRH_CNF14_Pos);
}

/**********************************************************************************************
 * Dummy Delay Function 
 **********************************************************************************************/

void delay(uint32_t time)
{
	uint32_t i = 0;
	for (i = 0; i < time; i++)
	{
		__NOP();
	}
}

