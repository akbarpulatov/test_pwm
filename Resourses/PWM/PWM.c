#include "PWM.h"

/********************************************************************************
 * PWM Initialization Function
 ********************************************************************************/

void PWM_Init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN
				 |  RCC_APB2ENR_AFIOEN;
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	
	//PA1
	GPIOA->CRL &= ~(GPIO_CRL_CNF1_Msk | GPIO_CRL_MODE1_Msk);
	GPIOA->CRL |= (0b11 << GPIO_CRL_MODE1_Pos) | (0b10 << GPIO_CRL_CNF1_Pos);
	
	//Delitel
	TIM2->PSC = 72 - 1;
	//Znacheniye perezagruzki
	TIM2->ARR = 1000 - 1;
	//Koefficient zapolneniya obnulyayem
	TIM2->CCR2 = 0;
	
	TIM2->CCMR1 = 0;   								//Reset value of register
	//Capture Compare Register
	TIM2->CCMR1 |= 0b00 << TIM_CCMR1_CC2S_Pos		//select channel for output
				|  0 << TIM_CCMR1_OC2FE_Pos			//Fast enable
				|  0 << TIM_CCMR1_OC2PE_Pos			//Preload enable
				|  0b110 << TIM_CCMR1_OC2M_Pos		//Output compare mode
				|  1 << TIM_CCMR1_OC2CE_Pos;   		//Output compare enable
	
	TIM2->CCER = 0;   								//Reset value of register
	TIM2->CCER |= 1 << TIM_CCER_CC2E_Pos			//OC2 signal is output on the corresponding output pin.
			   |  0 << TIM_CCER_CC2P_Pos;   			//OC1 active high
	
	TIM2->CR1 = 0;   								//Reset value of register
	TIM2->CR1 |= 1 << TIM_CR1_CEN_Pos				//Clock enable
			  |	 0 << TIM_CR1_DIR_Pos				//Direction
			  |	 0 << TIM_CR1_CMS_Pos;   			//Center-aligned mode
}
