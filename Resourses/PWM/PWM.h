/********************************************************************************
 * project     Tester For PCH_450V PCB											*
 *                                                                              *
 * file        ADC.h															*
 * author      Akbar Pulatov													*
 * date        12.02.2020                                                       *
 * copyright   Akbar Pulatov(C)													*
 * brief														                *
 *                                                                              *
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/
#pragma once 

#include "stm32f1xx.h"

/********************************************************************************
 * Define
 ********************************************************************************/   

/********************************************************************************
 * User enum
 ********************************************************************************/

/********************************************************************************
 * User typedef
 ********************************************************************************/

/********************************************************************************
 * Local function declaration
 ********************************************************************************/

void PWM_Init(void);


/********************************* END OF FILE **********************************/

